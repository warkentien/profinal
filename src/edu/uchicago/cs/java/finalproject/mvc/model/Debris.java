package edu.uchicago.cs.java.finalproject.mvc.model;

import java.awt.*;

/**
 * Created by sethf_000 on 11/27/2015.
 *
 * This class implements debris in the form of a circular explosion each time an asteroid is hit.
 */
public class Debris extends Sprite implements Movable {
    private int mExpire;
    private final int EXPIRE = 50;
    private Point mCenter;
    private int mRadiux;

    public Debris(Point point){
        super();

        //point from foe that is killed
        mCenter = point;
        setFadeValue(255);
        mExpire = EXPIRE;
        this.mRadiux = 1;

    }


    @Override
    public void move() {

        //remove debris from view after it expires
        if (mExpire == 0) {
            Cc.getInstance().getOpsList().enqueue(this,
                    CollisionOp.Operation.REMOVE);
        }
        //if it hasn't expired, manipulate radius to animate debris
        else {
            if (mExpire > EXPIRE / 2) {

                setRadius(getRadius() + 3);
            } else {
                setRadius(getRadius() - 3);
            }

            setExpire(getExpire() - 1);
        }
        mExpire--;
    }

    @Override
    //draws the explosion
    public void draw(Graphics g) {
        g.setColor(Color.LIGHT_GRAY);
        g.fillOval(mCenter.x -getRadius()/2, mCenter.y-getRadius()/2, getRadius(), getRadius());
        //g.fillOval(mCenter.x, mCenter.y, 50, 50);
    }

    @Override
    public Point getCenter() {
        return mCenter;
    }

    public void setRadius(int radiux) {
        mRadiux = radiux;
    }
    @Override
    public int getRadius() {
        return mRadiux;
    }

    @Override
    //returns name of team
    public Team getTeam() {
        return Team.DEBRIS;
    }
}
