package edu.uchicago.cs.java.finalproject.mvc.model;

import edu.uchicago.cs.java.finalproject.mvc.controller.Game;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * Created by sethf_000 on 12/6/2015.
 *
 * This class handles bombs dropped at the target location. It creates a red
 * circular explosion when dropped.
 */

public class Target implements Movable {

    public static final int EXPIRE = 50;
    private int mExpire;
    private int mRadiux;
    private Point mCenter;


    //constructor takes in mouse event
    public Target(MouseEvent e) {

        this.mExpire = EXPIRE;
        this.mRadiux = 1;
        this.mCenter = e.getPoint();


    }

    public void setRadius(int radiux) {
        mRadiux = radiux;
    }

    public int getExpire() {
        return mExpire;
    }

    public void setExpire(int expire) {
        mExpire = expire;
    }

    public void move() {

        //remove explosion from view after it expires
        if (mExpire == 0)
            Cc.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
        //if it hasn't expired, manipulate radius to animate explosion
        else {
            if (mExpire > EXPIRE / 2) {

                setRadius(getRadius() + 3);
            } else {
                setRadius(getRadius() - 3);
            }

            setExpire(getExpire() - 1);
        }
    }

    @Override
    //draw the explosion
    public void draw(Graphics g) {

        g.setColor(Color.RED);

        g.fillOval(getCenter().x -getRadius()/2, getCenter().y-getRadius()/2, getRadius(), getRadius());
    }

    //methods needed for the interface Movable
    @Override
    public Point getCenter() {
        return mCenter;
    }

    @Override
    public int getRadius() {
        return mRadiux;
    }

    @Override
    public Team getTeam() {
        return Team.FRIEND;
    }
}
